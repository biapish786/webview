package com.example.webview;

import android.app.Activity;
import android.app.ActionBar;
import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import android.os.Build;

public class MainActivity extends Activity implements OnClickListener{
private Button go,forward,back,refresh,clear;
private EditText urlText;
private WebView webView;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
     insilize();   
    }
        public void insilize(){
go=(Button) findViewById(R.id.btnGo);
 forward= (Button) findViewById(R.id.bForward);
 back= (Button) findViewById(R.id.bBack);
 refresh= (Button) findViewById(R.id.bRefresh);
 clear= (Button) findViewById(R.id.bClear);
 urlText= (EditText) findViewById(R.id.EditText);
 webView= (WebView) findViewById(R.id.WebView);
 go.setOnClickListener(this);
 back.setOnClickListener(this);
 forward.setOnClickListener(this);
 clear.setOnClickListener(this);
 refresh.setOnClickListener(this);
 urlText = (EditText) findViewById(R.id.EditText);
        }
        
  
@Override
public void onClick(View v) {
	// TODO Auto-generated method stub
	String text = urlText.getText().toString();
	switch (v.getId()){
	case R.id.btnGo:
		Toast.makeText(getApplicationContext(), "Loading", Toast.LENGTH_LONG).show();
		webView.loadUrl(text);
		break;
	case R.id.bBack:
		if(webView.canGoBack())
			webView.goBack();
		else 
			Toast.makeText(getApplicationContext(), "You cant go back", Toast.LENGTH_LONG).show();
		break;
	case R.id.bForward:
		if(webView.canGoForward())
			webView.goForward();
		else 
			Toast.makeText(getApplicationContext(), "You can t forward", Toast.LENGTH_LONG).show();
		break;
	case R.id.bClear:
	webView.clearHistory();
	break;
	case R.id.bRefresh:
		webView.reload();
		break;
	}
	
	
	
}

}